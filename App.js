import React from 'react';
import { StyleSheet, Text, View,Image } from 'react-native';
import Moment from 'moment';
import CountDown from 'react-native-countdown-component';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {timeNow: this.timeNow()};
  }
  render() {
    return (
      <View style={styles.container}>
        <Image
          style={{width: 100, height: 100}}
          source={{uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a7/React-icon.svg/2000px-React-icon.svg.png'}}
        />

        <Text style={styles.timeNow}>{this.state.timeNow}</Text>
     

       <CountDown
        until={6000}
        onFinish={() => alert('finished')}
        onPress={() => alert('hello')}
        size={20}
        digitBgColor={'#fff'}
        digitTxtColor={'#4CDAFE'}
      />


      </View>
    );
  }
  componentDidMount(){
    setInterval(() => {
      this.setState({
        timeNow: this.timeNow(),
      });
    }, 30);
  }
  timeNow() {
    return Moment().format('H:mm:ss');
  }
}
const styles = StyleSheet.create({
  timeNow: {
    textShadowColor: '#0AAFE6',
    textShadowOffset: {
      width: 0,
      height: 0
    },
    textShadowRadius: 10,
    fontSize: 70,
    color: '#daf6ff'
  },
  container: {
    flex: 1,
    backgroundColor: '#000',
    alignItems: 'center',
    justifyContent: 'center',
  },
});